## HTTP 

### Response

```javascript
var httpResponse = require('lunkis-util').httpResponse;

```

Example: with Express


### Success 200

```javascript
httpResponse.success(data, res);

```

```javascript
{
  "status": "OK",
  "result": {
  "email": "dgjsdgjasgjdasgjdasj@gmail.com",
  }
}
```

### Error 403 forbidden

```javascript
httpResponse.forbidden(data, res);

```

```javascript
{
  "status": 403,
  "error": "Forbidden",
  "message": {
  }
}
```
