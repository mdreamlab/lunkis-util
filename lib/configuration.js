var Configuration = function Configuration() {
  this.config = {};

  this.set = function(config, cb){
    this.config = config;
    cb(null, this.config);
  };

  this.get = function(){
    return this.config;
  };

};

Configuration.instance = null;

Configuration.getInstance = function(){
    if(this.instance === null)
       this.instance = new Configuration();

    return this.instance;

};



module.exports = Configuration.getInstance();
