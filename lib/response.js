var Http = require('http');

var internals = {

};

exports.create = function (statusCode, data, res) {
  if(statusCode===200){
    return internals.createSuccessMessage(statusCode, data, res);
  }
  else {
    return internals.createErrorMessage(statusCode, data, res);
  }
};


internals.createErrorMessage = function(code, message){

  var error =  Http.STATUS_CODES[code] || 'Unknown';

  var data = {
    'status':code,
    'error':error,
    'message':message
  };

  return data;

};

internals.createSuccessMessage = function(code, result){

  var status =  Http.STATUS_CODES[code] || 'Unknown';

  var data = {
    'status':status,
    'result':result
  };

  return data;

};


exports.success = function(data, res){
  res.status(200).send(exports.create(200, data, res));
};

exports.unauthorized  = function (data, res) {

     res.status(401).send(exports.create(401, data, res));
};


exports.forbidden = function (data, res) {

     res.status(403).send(exports.create(403, data, res));
};


exports.notFound = function (data, res) {

    res.status(404).send(exports.create(404, data, res));
};


exports.methodNotAllowed = function (data, res) {

    res.status(405).send(exports.create(405, data, res));
};


exports.notAcceptable = function (data, res) {

    res.status(406).send(exports.create(406, data, res));
};


exports.proxyAuthRequired = function (data, res) {

    res.status(407).send(exports.create(407, data, res));
};


exports.clientTimeout = function (data, res) {

    res.status(408).send(exports.create(408, data, res));
};


exports.conflict = function (data, res) {

    res.status(409).send(exports.create(409, data, res));
};


exports.resourceGone = function (data, res) {

    res.status(410).send(exports.create(410, data, res));
};


exports.lengthRequired = function (data, res) {

    res.status(411).send(exports.create(411, data, res));
};


exports.preconditionFailed = function (data, res) {

    res.status(412).send(exports.create(412, data, res));
};


exports.entityTooLarge = function (data, res) {

    res.status(413).send(exports.create(413, data, res));
};


exports.uriTooLong = function (data, res) {

   res.status(414).send(exports.create(414, data, res));
};


exports.unsupportedMediaType = function (data, res) {

    res.status(415).send(exports.create(415, data, res));
};


exports.rangeNotSatisfiable = function (data, res) {

    res.status(416).send(exports.create(416, data, res));
};


exports.expectationFailed = function (data, res) {

    res.status(417).send(exports.create(417, data, res));
};

exports.badData = function (data, res) {

    res.status(422).send(exports.create(422, data, res));
};


exports.tooManyRequests = function (data, res) {

    res.status(429).send(exports.create(429, data, res));
};

exports.error = function (data, res) {

    res.status(500).send(exports.create(500, data, res));
};
